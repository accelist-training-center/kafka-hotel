﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KafkaHotelProducer.Models;
using KafkaHotelProducer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KafkaHotelProducer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        private readonly ReservationService ReservationMan;

        public ReservationController(ReservationService reservationService)
        {
            ReservationMan = reservationService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ReservationModel reservationData)
        {
            var result = await ReservationMan.CreateReservation(reservationData);

            if (result == false)
            {
                return BadRequest("Failed to reserved a room");
            }

            return Ok();
        }


    }
}
