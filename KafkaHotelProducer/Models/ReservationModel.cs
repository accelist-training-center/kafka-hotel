﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KafkaHotelProducer.Models
{
    public class ReservationModel
    {
        public string CustomerName { get; set; }
        public string RoomType { get; set; }
    }
}
