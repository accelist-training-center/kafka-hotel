﻿using Confluent.Kafka;
using Confluent.Kafka.Admin;
using KafkaHotelProducer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace KafkaHotelProducer.Services
{
    public class ReservationService
    {
        private readonly ProducerConfig ProducerMan;
        private readonly String TopicName = "reservation-room";
        private readonly String BootstrapServers = "localhost:9092";

        public ReservationService()
        {
            ProducerMan = new ProducerConfig
            {
                BootstrapServers = this.BootstrapServers,
                ClientId = Dns.GetHostName(),
            };
        }

        public async Task<bool> CreateReservation(ReservationModel data)
        {
            var message = new Message<long, string>
            {
                Key = DateTime.UtcNow.Ticks,
                Value = JsonSerializer.Serialize(data)
            };

            var producer = new ProducerBuilder<long, string>(ProducerMan).Build();

            try
            {
                await ValidateTopic();
                var deliveryReport = await producer.ProduceAsync(TopicName, message);

                Console.WriteLine($"Message sent (value: '{message}'). Delivery status: {deliveryReport.Status}");
                if (deliveryReport.Status != PersistenceStatus.Persisted)
                {
                    // delivery might have failed after retries. This message requires manual processing.
                    Console.WriteLine($"ERROR: Message not ack'd by all brokers (value: '{message}'). Delivery status: {deliveryReport.Status}");
                }
            }
            catch (ProduceException<long, string> e)
            {
                Console.WriteLine($"Permanent error: {e.Message} for message (value: '{e.DeliveryResult.Value}')");
                return false;
            }

            return true;
        }

        public async Task ValidateTopic()
        {
            var adminConfig = new AdminClientConfig { BootstrapServers = this.BootstrapServers };
            var adminClient = new AdminClientBuilder(adminConfig).Build();

            var newTopic = new TopicSpecification
            {
                Name = TopicName,
                ReplicationFactor = 1,
                NumPartitions = 3
            };

            try
            {
                await adminClient.CreateTopicsAsync(new[] { newTopic });
            }
            catch (CreateTopicsException e) when (e.Results.Select(r => r.Error.Code).Any(el => el == ErrorCode.TopicAlreadyExists))
            {
                Console.WriteLine($"Topic {e.Results[0].Topic} already exists");
            }
        }
    }
}
