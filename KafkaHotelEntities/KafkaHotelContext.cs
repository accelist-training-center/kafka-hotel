﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace KafkaHotelEntities
{
    public partial class KafkaHotelContext : DbContext
    {
        public KafkaHotelContext()
        {
        }

        public KafkaHotelContext(DbContextOptions<KafkaHotelContext> options)
            : base(options)
        {
        }

        public virtual DbSet<KitchenReservation> KitchenReservation { get; set; }
        public virtual DbSet<RoomReservation> RoomReservation { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<KitchenReservation>(entity =>
            {
                entity.Property(e => e.CustomerName).IsUnicode(false);
            });

            modelBuilder.Entity<RoomReservation>(entity =>
            {
                entity.Property(e => e.CustomerName).IsUnicode(false);
            });
        }
    }
}
