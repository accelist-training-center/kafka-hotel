﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KafkaHotelEntities
{
    public partial class RoomReservation
    {
        public int RoomReservationId { get; set; }
        [StringLength(500)]
        public string CustomerName { get; set; }
        public DateTimeOffset? BookingDate { get; set; }
    }
}
