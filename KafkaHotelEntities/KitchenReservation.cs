﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KafkaHotelEntities
{
    public partial class KitchenReservation
    {
        public int KitchenReservationId { get; set; }
        [StringLength(500)]
        public string CustomerName { get; set; }
        public DateTimeOffset? ServingDate { get; set; }
    }
}
