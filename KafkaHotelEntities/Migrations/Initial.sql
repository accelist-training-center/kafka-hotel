CREATE DATABASE KafkaHotel

GO

USE KafkaHotel

CREATE TABLE KitchenReservation
(
	KitchenReservationId INT IDENTITY PRIMARY KEY,
	CustomerName VARCHAR(500),
	ServingDate DATETIMEOFFSET,
)

CREATE TABLE RoomReservation
(
	RoomReservationId INT IDENTITY PRIMARY KEY,
	CustomerName VARCHAR(500),
	BookingDate DATETIMEOFFSET,
)