﻿using Confluent.Kafka;
using Confluent.Kafka.Admin;
using KafkaHotelEntities;
using KafkaKitchenConsumer.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KafkaKitchenConsumer
{
    public class KitchenService
    {
        private readonly ConsumerConfig ConsumerConfigMan;
        private readonly string TopicName = "reservation-room";
        private readonly string BootstrapServers = "localhost:9092";

        public KitchenService()
        {
            ConsumerConfigMan = new ConsumerConfig
            {
                BootstrapServers = this.BootstrapServers,
                //Set the group id
                GroupId = "kitchen",
                AutoOffsetReset = AutoOffsetReset.Earliest,
                MaxPollIntervalMs = 300000,
                AllowAutoCreateTopics = true,
                EnableAutoCommit = false, // (the default)
                EnableAutoOffsetStore = false, // (the default)
            };
        }

        public async Task ReceiveReservation(string connString)
        {
            var consumer = new ConsumerBuilder<Ignore, string>(ConsumerConfigMan).Build();
            var cancelled = false;

            await this.ValidateTopic();

            //Subscribe to specific topic
            consumer.Subscribe(TopicName);

            try
            {
                while (!cancelled)
                {
                    var consumeResult = consumer.Consume(TimeSpan.FromMilliseconds(250000));

                    // handle consumed message.
                    var message = consumeResult?.Message?.Value;
                    if (message == null)
                    {
                        continue;
                    }

                    Console.WriteLine($"Received: {consumeResult.Message.Key}:{message} from partition: {consumeResult.Partition.Value}");

                    var reservationData = JsonConvert.DeserializeObject<ReservationModel>(message);

                    var optionsBuilder = new DbContextOptionsBuilder<KafkaHotelContext>()
                    .UseSqlServer(connString);

                    using (var context = new KafkaHotelContext(optionsBuilder.Options))
                    {

                        var newReservation = new KitchenReservation()
                        {
                            CustomerName = reservationData.CustomerName,
                            ServingDate = DateTimeOffset.UtcNow.AddHours(6)
                        };

                        context.KitchenReservation.Add(newReservation);
                        await context.SaveChangesAsync();
                    }

                    // By default setted true.
                    consumer.Commit(consumeResult);
                    consumer.StoreOffset(consumeResult);
                }
            }
            catch (KafkaException e)
            {
                Console.WriteLine($"Consume error: {e.Message}");
                Console.WriteLine("Exiting producer...");
            }
            finally
            {
                consumer.Close();
            }
        }

        public async Task ValidateTopic()
        {
            var adminConfig = new AdminClientConfig { BootstrapServers = this.BootstrapServers };
            var adminClient = new AdminClientBuilder(adminConfig).Build();

            var newTopic = new TopicSpecification
            {
                Name = TopicName,
                ReplicationFactor = 1,
                NumPartitions = 3
            };

            try
            {
                await adminClient.CreateTopicsAsync(new[] { newTopic });
            }
            catch (CreateTopicsException e) when (e.Results.Select(r => r.Error.Code).Any(el => el == ErrorCode.TopicAlreadyExists))
            {
                Console.WriteLine($"Topic {e.Results[0].Topic} already exists");
            }
        }
    }
}