﻿using System;
using System.Threading.Tasks;

namespace KafkaKitchenConsumer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Starting Kitchen Service consumer application");

            var roomMan = new KitchenService();
            await roomMan.ReceiveReservation("Data Source=localhost;Initial Catalog=KafkaHotel;User ID=sa;Password=;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            Console.WriteLine("Closing Kitchen Service consumer application");
            Console.ReadKey();
        }
    }
}
