﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KafkaRoomConsumer.Models
{
    public class ReservationModel
    {
        public string CustomerName { get; set; }
        public string RoomType { get; set; }
    }
}
