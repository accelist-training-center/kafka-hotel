﻿using System;
using System.Threading.Tasks;

namespace KafkaRoomConsumer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Starting Room Service consumer application");

            var roomMan = new RoomService();
            await roomMan.ReceiveReservation("Data Source=localhost;Initial Catalog=KafkaHotel;User ID=sa;Password=;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            Console.WriteLine("Closing Room Service consumer application");
            Console.ReadKey();
        }
    }
}
