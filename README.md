# Kafka Hotel

A simple hotel reservation system for ASP.NET Core 2.1 + Kafka demonstration project.

## Installation
### Cloning the Repository
Clone the repository using this following command:
```
git clone https://gitlab.com/accelist-training-center/kafka-hotel.git
```
### Developer Tools
To begin the development of the application, you must install these following softwares:
| Name | Version | Type | DL Link | Notes |
| ---- | ------- | ---- | ------- | ----- |
| .NET Core | 2.1 | SDK | https://dotnet.microsoft.com/download/dotnet/2.1 | For Web API Producer |
| .NET Core | 3.1 | SDK | https://dotnet.microsoft.com/download/dotnet/3.1 | For Consumer Console App |
| SQL Server Management Studio |  | Database |  |  |
| Visual Studio | 2019 | IDE | | (Optional) you could use other IDE or code editor. |
| Visual Studio Code | | IDE | | (Optional) you could use other IDE or code editor. |

### Build & Run the Application
To build and run the container that contains Kafka, Zookeeper, which all included in `docker-compose.yml` file:
```shell
docker-compose up -d
```
> _Author's Note:_ It might take a while for downloading the image.

To build and run the .NET Application you can execute via your own IDE or using .NET CLI from each project directory. Don't forget to adjust the SQL Server connection string on each consumer project's `Program.cs`:
```shell
dotnet run
```
### Configuration
All configuration settings could be configured using the `appsettings.json` file, **BUT** for personal development use, you must configure the configuration file using **secrets**.
## Hosting
For hosting, you must install these following softwares:
| Name | Version | Type | DL Link | Notes |
| ---- | ------- | ---- | ------- | ----- |
| ASP.NET Core Runtime | 2.1 | Runtime | https://dotnet.microsoft.com/download/dotnet/2.1 | (Optional) If you don't want to use Docker  |
| ASP.NET Core Runtime | 3.1 | Runtime | https://dotnet.microsoft.com/download/dotnet/3.1 | (Optional) If you don't want to use Docker  |
| Docker| 4.3.2 | Tools| https://www.docker.com/products/docker-desktop |To Host the Kafka, Zookeeper, and Kafdrop|
| SQL Server | | Database | | (Optional) If you don't want to use Docker |